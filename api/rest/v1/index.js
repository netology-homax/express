const express = require('express');
let app = module.exports = express();

let apiV1Users = require('./users');

app.use('/users', apiV1Users);
