const express = require('express');
let app = module.exports = express();

const User = require('../../../../model/User');
const Storage = require('../../../../storage');

let validFields = ['id', 'name', 'score'];

app.get('/', function(req, res) {
  let {limit, offset, fields} = req.query;
  let result = Storage.get();

  if (offset) {
    result = getOffset(result, offset);
  }
  if (limit) {
    result = getLimit(result, limit);
  }
  if (fields) {
    result = getFields(result, fields);
  }

  res.json(result);
});

app.post('/', function(req, res) {
  let name = req.body.name;
  let score = req.body.score;
  let user = new User(name, score);
  Storage.create(user);
  res.json(user);
});

app.put('/:id', function(req, res) {
  let id = req.params.id;
  let name = req.body.name;
  let score = req.body.score;
  let user = Storage.get(id);
  if (user) {
    user.name = name;
    user.score = score;
  }
  res.json(user);
});

app.delete('/:id', function(req, res) {
  let id = req.params.id;
  let deleted = Storage.delete(id);
  res.json(deleted);
});

app.delete('/', function(req, res) {
  let deleted = Storage.delete();
  res.json(deleted);
});

function getFields(users, fields) {
  let result = users;
  let flds = fields.split(',');
  flds = flds.filter(field => validFields.indexOf(field) !== -1);
  if (flds.length > 0) {
    result = result.map(user => {
        return flds.reduce((res, fld) => {
            res[fld] = user[fld];
            return res;
        }, {});
    });
  }
  return result;
}

function getOffset(users, offset) {
  return offset < users.length ? [...users.slice(offset)] : [];
}

function getLimit(users, limit) {
  return users.length <= limit ? users : [...users.slice(0, limit)];
}
